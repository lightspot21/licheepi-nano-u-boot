# U-Boot v2021.04 for LicheePi Nano

This is a port of the Das U-Boot bootloader for the LicheePi Nano.

Based on [florpor's](https://github.com/florpor) [v.2020.01 port](https://github.com/florpor/u-boot/)
and hand-patched by [lightspot21](https://gitlab.com/lightspot21) for v.2021.04.

The port was made for the Bittboy v3.5 emulation console, but it should be generic enough for any Allwinner
F1C100s-based board.


# Building

- Get an ARM cross-compiler with embedded Linux (not bare-metal) support (target triplet should be `arm-<VENDOR>-linux-<LIBC><ABI>-` or something like that)
- Make sure the cross-compiler is on your PATH.
- Configure with `ARCH=arm CROSS_COMPILE=arm-linux- make licheepi_nano_defconfig` for booting from SD card
(Note: SPI flash booting is also supported, but not tested myself. Use `licheepi_nano_spiflash_defconfig` if you want that.)
- After configuration, compile with `ARCH=arm CROSS_COMPILE=arm-linux- make`
- Your bootloader is the file named `u-boot-sunxi-with-spl.bin`.

# Contributing

Merge requests/help keeping this port updated to the latest U-Boot (or even help in upstreaming it to the original project) will be especially appreciated!

## Branch naming policy

There are 2 naming schemes at play:

- `tag`-`licheepi_nano` where `tag` stands for the corresponding **stable** version. This is considered a stable version of the bootloader, and the intent
for these branches is to be applied onto the clean uboot version as a patch.
- `tag`-`licheepi_nano`-`devel`: same as above, but unstable. Used for development purposes.
